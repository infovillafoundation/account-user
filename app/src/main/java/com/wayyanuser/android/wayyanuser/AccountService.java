package com.wayyanuser.android.wayyanuser;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by aztunwin on 18/12/15.
 */
public interface AccountService {
    @POST("/accounts")
    Call<Account>
    saveAccount(@Body Account account);
}
